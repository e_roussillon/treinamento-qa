# coding: utf-8
######### DADO #########

When(/^que estou na tela de consulta de CEP$/) do
  @page = page(ConsultarScreen).await(timeout: 5)
end

######### QUANDO #########

When(/^digitar um CEP (válido|inválido|inexistente)$/) do |tipo_cep|
  @page.digitar_cep CEP[tipo_cep.to_sym][:numero]
end

When(/^tocar o botão buscar/) do
  @page.tocar_botao_buscar
end

######### ENTãO #########

When(/^poderei ver (?:o endereço|a mensagem) do CEP (válido|inválido|inexistente)$/) do |tipo_cep|
  @page.is_on_page? CEP[tipo_cep.to_sym][:endereco]
end