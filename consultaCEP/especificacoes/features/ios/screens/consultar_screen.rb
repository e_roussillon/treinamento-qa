# coding: utf-8
class ConsultarScreen < IOSScreenBase

  # Identificador da tela
  trait(:trait)                 { "* marked:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'CONSULTAR_SCREEN' }
  element(:campo_cep)           { 'CAMPO_CEP' }
  element(:campo_buscar)        { 'Buscar' }

  action(:tocar_botao_buscar) {
    touch_screen_element campo_buscar
  }

  # @param [String] cep
  def digitar_cep(cep)
    enter cep, campo_cep
  end

end
