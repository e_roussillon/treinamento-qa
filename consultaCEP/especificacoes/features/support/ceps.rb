# coding: utf-8
CEP = {
  válido: {
    numero: "04571923",
    endereco: "Sansão Alves dos Santos"
  },
  inválido: {
      numero: "",
      endereco: "CEP inválido"
  },
  inexistente: {
      numero: "18500000",
      endereco: "CEP não encontrado"
  }
}
